import socketIO from 'socket.io';
import * as _ from 'lodash';

import RoomService from './services/RoomService';
import { Room } from './services/Room';
import { User } from './services/User';
import UserService from './services/UserService';

const enum UserActions {
  CREATE_ROOM = 'CREATE_ROOM',
  JOIN_ROOM = 'JOIN_ROOM',
  UPDATE_USER = 'UPDATE_USER',
  KEYBOARD_PRESS = 'KEYBOARD_PRESS'
}

export const enum GlobalActions {
  UPDATE_ROOMS = 'UPDATE_ROOMS',
  UPDATE_ROOM = 'UPDATE_ROOM',
  SEND_ERROR = 'SEND_ERROR',
  SEND_BOT_COMMENT = 'SEND_BOT_COMMENT'
}

const enum ErrorActions {
  USERNAME_EXISTS = 'USERNAME_EXISTS',
  ROOMNAME_EXISTS = 'ROOMNAME_EXISTS'
}

export type Actions = UserActions | GlobalActions | ErrorActions;
type EmitSource = socketIO.Server | socketIO.Socket;

export default (io: socketIO.Server): void => {
  const roomService = new RoomService();
  const userService = new UserService();

  const emitToRoomCurried = _.curry(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (emitSource: EmitSource, roomname: string, actionType: Actions, args: any[]) => {
      emitSource.to(roomname).emit(actionType, ...args);
    });
  const emitCurried = _.curry(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (emitSource: EmitSource, actionType: Actions, args: any[]) => {
      emitSource.emit(actionType, ...args);
    });
  const emitToRoomByIO = emitToRoomCurried(io);
  const emitToLobbyByIO = emitToRoomByIO('lobby');

  io.on('connection', socket => {
    const emitBySocket = emitCurried(socket);
    const emitErrorToSocket = emitBySocket(GlobalActions.SEND_ERROR);
    const emitToRoomBySocked = emitToRoomCurried(socket);

    let user: User;
    try {
      const { username } = socket.handshake.query;
      user = new User(username);
      userService.addUser(user);
    } catch (error) {
      emitErrorToSocket([ErrorActions.USERNAME_EXISTS, error.message]);
      return;
    }

    // 'lobby' - room for current-not-in-any-room users
    socket.join('lobby');
    // Show rooms for new user
    emitBySocket(GlobalActions.UPDATE_ROOMS)([roomService.getAllRooms()]);

    socket.on(UserActions.CREATE_ROOM, (name: string) => {
      let room: Room;
      try {
        room = new Room(emitToRoomByIO(name), name, []);

        room.addUser(user);
        socket.join(room.name);
        // socket.rooms
        socket.leave('lobby');

        roomService.addRoom(room);
      } catch (error) {
        emitErrorToSocket([ErrorActions.ROOMNAME_EXISTS, error.message]);
        return;
      }

      emitBySocket(UserActions.JOIN_ROOM)([room]);
      // Created new room - update rooms on client
      emitToLobbyByIO(GlobalActions.UPDATE_ROOMS)([roomService.getAllRooms()]);
    })

    socket.on(UserActions.JOIN_ROOM, (roomname: string) => {
      const room = roomService.getRoomByRoomname(roomname);
      if (room) {
        room.addUser(user);
        socket.leave('lobby');
        socket.join(room.name);
      }
      // Render room on client
      emitBySocket(UserActions.JOIN_ROOM)([room]);
      // New user in room - Update room for users in this room (if any)
      emitToRoomBySocked(roomname)(GlobalActions.UPDATE_ROOM)([room]);
      // Update user-counter for one room
      emitToLobbyByIO(GlobalActions.UPDATE_ROOMS)([roomService.getAllRooms()]);
    })

    socket.on(UserActions.UPDATE_USER, (newUser: User) => {
      try {
        // Find room in which user is and update Room and User
        const room = roomService.getRoomByUsername(newUser.username) as Room;
        room.updateUserByUsername(newUser.username, newUser);
        roomService.updateRoomByRoomname(room.name, room);
        userService.updateUserbyUsername(newUser.username, newUser);
      } catch (error) {
        console.error(error);
        return;
      }

      const room = roomService.getRoomByUsername(user.username) as Room;
      // User toggle READY | NON_READY in room - Update room for users in this room (if any)
      emitToRoomByIO(room.name)(GlobalActions.UPDATE_ROOM)([room]);
      // Update user-counter for one room (this action triggers in READY | NON-READY toggle)
      emitToLobbyByIO(GlobalActions.UPDATE_ROOMS)([roomService.getAllRooms()]);
    })

    socket.on(UserActions.KEYBOARD_PRESS, (username: string, keycode: string) => {
      const room = roomService.getRoomByUsername(username) as Room;
      room.game.handleKeypress(username, keycode);
      roomService.updateRoomByRoomname(room.name, room);
    })

    socket.on('disconnecting', () => {
      const { username } = socket.handshake.query;
      userService.removeUserByUsername(username);
      // Get roomname of socket (filter because Object.keys(socket.rooms) also return unique for every socket)
      const roomname =  Object.keys(socket.rooms).filter(room => room !== socket.id)[0];
      const room = roomService.getRoomByRoomname(roomname);
      // Dont sure that each of these 2 lines are needed :/ 
      room?.removeUserByUsername(username);
      roomService.removeUserByUsername(username);

      emitToRoomByIO(roomname)(GlobalActions.UPDATE_ROOM)([room]);
      socket.join('lobby');
      // Update room-list state for all users in lobby
      emitToLobbyByIO(GlobalActions.UPDATE_ROOMS)([roomService.getAllRooms()]);
    })
  });
};