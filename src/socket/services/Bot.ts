import { texts } from "../../api/data/data";
import { facts } from "../../api/data/facts";
import { 
  COMMENT_NOTIFICATION_COOLDOWN,
  SECONDS_FOR_GAME,
  CHARS_BEFORE_FINISH
} from "../../config";
import { GameStates, GameStatesProperties, GameState, IGameWinner } from "./Game";
import { IGameUser } from "./Game";

const transports = ['новеньком ferrari', 'белом BMW', 'модной Тесле']

const comments = {
  about: `На улице сейчас немного пасмурно, но на Львов Арена сейчас просто замечательная атмосфера:
    двигатели рычат, зрители улыбаются а гонщики едва заметно нервничают и готовят своих железных коней к заезду.
    А комментировать всё это действо буду я, Эскейп Энтерович и я рад вас приветствовать со словами Доброго Вам дня, господа!`,
  greetings: (users: IGameUser[]) => {
    let greetings = '';
    for (let i = 0; i < users.length; i++) {
      const transport = transports[Math.floor(Math.random() * transports.length)];
      greetings += `На позиции ${i + 1} у нас ${users[i].userInfo.username} на ${transport}; `;
    }
    greetings += 'Всем удачной гонки!';
    return greetings;
  },
  results: (winners: IGameWinner[]) => {
    let results = '';
    for (let i = 0; i < Math.min(3, winners.length); i++) {
      const winner = winners[i];
      results += `${i + 1} - ${winner.userInfo.username} (${winner.timeSpend} секунд потрачено)`;
    }
    return results;
  }
}

// true if we notificate user with this type of notification
interface IUserNotification {
  closeToFinish: boolean;
  finished: boolean;
}

export class Bot {
  private usersNotificationsMap: Record<string, IUserNotification>;
  private isGreeted = false;
  private isAbout = false;

  constructor(
    private emit: (comment: string) => void
  ) {
    this.usersNotificationsMap = {};
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  gameHandler(target: GameState, property: GameStatesProperties, value: any): void {
    switch (property) {
      case 'state': return this.stateHandler(value as GameStates, target);
    }
  }

  stateHandler(state: GameStates, gameState: GameState): void {
    switch (state) {
      case GameStates.START_TIMER:    return this.sendAbout();
      case GameStates.GAME:           return this.processStateChange(gameState);
      case GameStates.GAME_FINISHED:  return this.emit(comments.results(gameState.winners))
    }
  }

  private sendAbout() {
    if (!this.isAbout) {
      this.emit(comments.about);
      this.isAbout = true;
    }
  }

  private processStateChange(state: GameState): void {
    for (const user of state.users) {
      const { username } = user.userInfo;
      if (!this.usersNotificationsMap[username]) {
        this.usersNotificationsMap[username] = {
          closeToFinish: false,
          finished: false
        };
      }
    }

    if (!this.isGreeted) {
      this.emit(comments.greetings(state.users));
      this.isGreeted = true;
    }

    const cooldown = this.getSecondsFromStart(state);
    if (cooldown % COMMENT_NOTIFICATION_COOLDOWN === 0 && cooldown > 0) {
      let gameStateComment = '';
      const users = state.users.sort((user1, user2) => user2.progress - user1.progress);
      for (let i = 0; i < users.length; i++) {
        const user = users[i];
        const { username } = user.userInfo;
        gameStateComment += `На позиции ${i + 1} у нас ${username} `;
        if (i + 1 < users.length) {
          const distance = users[i].progress - users[i + 1].progress;
          gameStateComment += `Расстояние ${distance}; `;
        }
      }
      this.emit(gameStateComment);
      return;
    }

    if (cooldown % COMMENT_NOTIFICATION_COOLDOWN === COMMENT_NOTIFICATION_COOLDOWN / 2 && cooldown > 0) {
      const fact = facts[Math.floor(Math.random() * facts.length)];
      this.emit(fact);
      return;
    }

    for (const user of state.users) {
      const { username } = user.userInfo;
      const text = texts[state.textId];
      const left = text.length - user.progress;
      if (left <= CHARS_BEFORE_FINISH && !this.usersNotificationsMap[username].closeToFinish) {
        const message = `${user.userInfo.username} осталось ${left} символов до конца`;
        this.usersNotificationsMap[username].closeToFinish = true;
        this.emit(message);
        continue;
      }
      if (left === 0 && !this.usersNotificationsMap[username].finished) {
        const message = `${user.userInfo.username} финишировал!`;
        this.usersNotificationsMap[username].finished = true;
        this.emit(message);
        continue;
      }
    }
  }

  private getSecondsFromStart(state: GameState): number {
    const { gameSecondsLeft } = state;
    return SECONDS_FOR_GAME - gameSecondsLeft;
  }

}