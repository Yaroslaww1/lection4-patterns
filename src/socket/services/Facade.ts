import { Actions, GlobalActions } from "..";
import { Bot } from "./Bot";
import { Game } from "./Game";
import { Room } from "./Room";

export class Facade {
  constructor(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private emit: (action: Actions) => (args: any[]) => void,
    private getRoomData: () => Room
  ) {}

  public createGame(): Game {
    const emitComment = (comment: string) => {
      this.emit(GlobalActions.SEND_BOT_COMMENT)([comment]);
      // console.log('EMITTED COMMENT', comment);
    }

    console.log('BOT CREATED')
    const bot = new Bot(emitComment);

    const emitGameState = (game: Game) => {
      this.emit(GlobalActions.UPDATE_ROOM)([{ ...this.getRoomData(), game }]);
    }
    const game = new Game(emitGameState, bot);
    game.registerRoomDataGetter(this.getRoomData);
    return game;
  }
}