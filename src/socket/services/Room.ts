import { User } from "./User";
import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../../config'; 
import { Game } from "./Game";
import { Facade } from "./Facade";
import { Actions } from "..";

export interface IRoomState {
  game: Game,
  name: string,
  users: User[]
}

export class Room {
  public isAvailable = true;
  public game: Game;
  private gameCreator: Facade;
  constructor(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private emit: (action: Actions) => (args: any[]) => void,
    public name: string,
    public users: User[],
  ) {
    this.gameCreator = new Facade(this.emit, () => this);
    this.game = this.gameCreator.createGame();
  }

  private startGame(): void {
    this.isAvailable = false;
    this.game.start()
      .then(() => {
        // Game finished, create new game
        this.game = this.gameCreator.createGame();
        this.users = this.users.map(user => {
          const newUser = new User(user.username);
          return newUser;
        });
        this.isAvailable = true;
      })
  }

  private canGameBeStarted(): boolean {
    const notReadyUsers = this.users.filter(user => !user.isReady);
    if (this.users.length > 0 && notReadyUsers.length === 0 && !this.game.isStarted())
      return true;
    else
      return false;
  }

  updateUserByUsername(username: string, newUser: User): void {
    this.users = this.users.map(user => {
      if (user.username === username)
        Object.assign(user, newUser);
      return user;
    });

    if (this.canGameBeStarted())
      this.startGame();
  }

  addUser(user: User): void {
    this.users.push(user);
    if (this.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
      this.isAvailable = false;
    }

    if (this.canGameBeStarted()) 
      this.startGame();
  }

  removeUserByUsername(username: string): void {
    this.users = this.users.filter(user => {
      return user.username !== username;
    })
    if (this.users.length > 0)
      this.isAvailable = true;

    this.game.state.users = this.game.state.users.filter(
      user => user.userInfo.username !== username
    );

    if (this.canGameBeStarted())
      this.startGame();
  }
}