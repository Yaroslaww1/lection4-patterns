import { Request, Response, NextFunction } from 'express';

export default (err: Error, req: Request, res: Response, next: NextFunction) => {
  console.error(err);
  if (res.headersSent) { // http://expressjs.com/en/guide/error-handling.html
    next(err);
  } else {
    const { message = '' } = err;
    res.status(500).send({ message });
  }
};
