import express from 'express';
import loginRoutes from './loginRoutes';
import gameRoutes from './gameRoutes';

// register all routes
export default (app: express.Application): void => {
  app.use('/login', loginRoutes);
  app.use('/game', gameRoutes);
};
