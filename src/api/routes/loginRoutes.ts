import { Router, Request, Response, NextFunction } from 'express';
import path from 'path';
import { HTML_FILES_PATH } from '../../config';
import asyncHandler from '../helpers/asyncHandler';

const router = Router();

const loginHandler = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const page = path.join(HTML_FILES_PATH, "login.html");
  res.sendFile(page);
});

router
  .get(
    '/',
    loginHandler
  )


export default router;