import { Router, Request, Response, NextFunction } from 'express';
import path from 'path';
import { HTML_FILES_PATH } from '../../config';
import asyncHandler from '../helpers/asyncHandler';
import { texts } from '../data/data';

const router = Router();

const gameHandler = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const page = path.join(HTML_FILES_PATH, "game.html");
  res.sendFile(page);
});

const gameTextsHandler = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const id = parseInt(req.params.id);
  const text = texts[id];
  res.json({
    text
  });
});

router
  .get(
    '/',
    gameHandler
  )

  .get(
    '/texts/:id',
    gameTextsHandler
  );


export default router;