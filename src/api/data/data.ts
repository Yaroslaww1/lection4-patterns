export const texts = [
  'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempora saepe, ipsa autem quas praesentium id cumque numquam, quos consectetur suscipit assumenda sed aperiam corporis debitis iure omnis delectus. Voluptatum quia magnam tempore neque natus cumque, asperiores ducimus eaque alias tempora! Modi blanditiis et sapiente ad consequatur, fuga tempora excepturi earum.',
  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam expedita officiis culpa blanditiis saepe debitis eos voluptatibus, placeat non quasi earum magni, fugit nobis nulla itaque facere aspernatur quidem numquam assumenda doloribus eaque dicta! Ab iste ipsum nihil nostrum reprehenderit?',
  'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ab eum harum fugiat a accusantium sequi repudiandae possimus asperiores! Asperiores animi explicabo quod facilis repellat nesciunt. Quas soluta, assumenda deserunt corrupti qui consectetur aliquid quae cupiditate facilis porro placeat, quisquam explicabo nam minima? Minima, minus voluptatibus!'
];

export default { texts };