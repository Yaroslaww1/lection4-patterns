import { highlight } from "./helper.mjs";
import { emitKeyboardEvent } from "./game.mjs";
import { emitUpdateUser } from "./game.mjs";
import { domHelper } from "./helper.mjs";

const gloabalUsername = sessionStorage.getItem("username");

export let game = undefined;

function keyboardEventListener (event) {
  emitKeyboardEvent(event.key);
}

class Game {
  text = '';
  isStarted = false;
  constructor (textId) {
    fetch(`http://localhost:3002/game/texts/${textId}`, {
      method: 'GET'
    })
    .then(data => data.json())
    .then(data => {
      this.text = data.text;
    });
  }

  process(gameInfo) {
    if (!this.isStarted) {
      this.isStarted = true;
      document.addEventListener('keypress', keyboardEventListener);
      domHelper
        .hideById('room-start-timer')
        .showById('room-seconds-left')
        .showById('room-text');
    }
    const { gameSecondsLeft: secondsLeft } = gameInfo;
   
    // Highlight progress bar
    const userProgress = gameInfo.users.find(({ userInfo }) => userInfo.username === gloabalUsername).progress;
    domHelper
      .setInnerHTMLById('room-text', highlight(this.text, userProgress))
      .setInnerHTMLById('room-seconds-left', `${secondsLeft} seconds left`)

    for (const user of gameInfo.users) {
      updateUserProgress(user, this.text);      
    }
  }

  end() {
    this.isStarted = false;
    game = undefined;

    document.removeEventListener('keypress', keyboardEventListener);
    domHelper
      .hideById('room-start-timer')
      .hideById('room-seconds-left')
      .hideById('room-text')
      .showById('room-back-button', 'flex')
      .showById('room-ready-button', 'flex');
    emitUpdateUser({
      username: gloabalUsername,
      isReady: false
    })
  }
}

function updateUserProgress(user, text) {
  const { username } = user.userInfo;
  const progress = Math.floor(user.progress * 100 / text.length); 
  domHelper
    .setStylepropertyById(`${username}-user-progress`, 'width', progress + '%')
  if (user.progress === text.length) {
    domHelper
      .setStylepropertyById(`${username}-user-progress`, 'backgroundColor', 'greenyellow')
  }
}

export function createGame(textId) {
  game = new Game(textId);
  return game;
}