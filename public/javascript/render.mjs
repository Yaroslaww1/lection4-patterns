import { createElement } from "./helper.mjs";
import { emitUpdateUser } from "./game.mjs";

import { game, createGame } from './gameFactory.mjs';
import { currentUser, updateUser } from './userState.mjs';
import { domHelper } from "./helper.mjs";

const gloabalUsername = sessionStorage.getItem("username");

const roomBackButton = document.getElementById('room-back-button');
const roomReadyButton = document.getElementById('room-ready-button');

roomBackButton.addEventListener('click', () => {
  window.location.replace("http://localhost:3002"); 
});

roomReadyButton.addEventListener('click', () => {
  updateUser({
    ...currentUser,
    isReady: !currentUser.isReady
  });
  emitUpdateUser(currentUser);
});

// Renders list of rooms
export const renderRoomsList = (createRoom, joinRoom, rooms) => {
  const createRoomButton = document.getElementById('create-room');
  createRoomButton.addEventListener('click', createRoom);

  domHelper
    .showById('rooms-list-wrapper', 'flex')
    .hideById('room-wrapper')
  
  const allRooms = rooms.map(room => {
    const { name, users } = room;
    const roomElement = createElement({
      tagName: "div",
      className: "room-item no-select",
      attributes: { id: name }
    })
    const roomNameElement = createElement({
      tagName: "h3",
      className: "room-item-name no-select"
    });
    roomNameElement.innerHTML = `${name}`;
    const roomCounterElement = createElement({
      tagName: "h4",
      className: "room-item-counter no-select"
    })
    roomCounterElement.innerHTML = `${users.length} users connected`;
    const joinButtonElement = createElement({
      tagName: "button",
      attributes: { id: `${name}-join-button` }
    })
    joinButtonElement.innerHTML = 'JOIN'
    joinButtonElement.addEventListener('click', () => {
      joinRoom(name);
      renderRoom(room);
    });
    roomElement.append(roomCounterElement, roomNameElement, joinButtonElement);
    return roomElement;
  });

  domHelper
    .setInnerHTMLById('rooms-container', '')
    .appendById('rooms-container', allRooms)
}

// Get user HTML element
const getUserElement = user => {
  const { username, isReady } = user;
  const userElement = createElement({
    tagName: "div",
    className: "user",
    attributes: { id: `${username}-user-wrapper` }
  });
  // Contain status && name
  const userHeaderElement = createElement({
    tagName: "div",
    className: "user-header",
    attributes: { id: `${username}-user-header` }
  });
  const userUsernameElement = createElement({
    tagName: "div",
    className: "user-username",
    attributes: { id: `${username}-user-username` }
  })
  userUsernameElement.innerHTML = `${username} ${username === gloabalUsername ? '(you)' : ''}`

  const userReadyElement = createElement({
    tagName: "div",
    className: `user-ready-status ${isReady === true ? 'green' : 'red'}`,
    attributes: { id: `${username}-user-ready-status` }
  });

  const userProgressWrapperElement = createElement({
    tagName: "div",
    className: "user-progress-wrapper",
    attributes: { 
      id: `${username}-user-progress-wrapper`
    }
  });
  const userProgressElement = createElement({
    tagName: "div",
    className: "user-progress",
    attributes: { 
      id: `${username}-user-progress`
    }
  });
  userProgressWrapperElement.append(userProgressElement);
  userHeaderElement.append(userReadyElement, userUsernameElement);
  userElement.append(userHeaderElement, userProgressWrapperElement);
  return userElement;
}

export const renderRoom = (room) => {
  domHelper
    .hideById('rooms-list-wrapper')
    .showById('room-wrapper', 'flex')

  // Get user which logged in
  const user = room.users.filter(user => user.username === gloabalUsername)[0];
  updateUser(user);
  if (!currentUser) 
    return;

  if (currentUser.isReady) 
    domHelper
      .setInnerHTMLById('room-ready-button', 'NOT READY')
  else 
    domHelper
      .setInnerHTMLById('room-ready-button', 'READY')

  domHelper
    .setInnerHTMLById('room-roomname', `<h1>${room.name}</h1>`)
  
  const allUsersElements = room.users.map(user => getUserElement(user));

  domHelper
    .setInnerHTMLById('room-users-container', '')
    .appendById('room-users-container', allUsersElements)

  const { state, ...value } = room.game.state;
  if (!state) 
    return;

  function gameHandler(value) {
    if (!game)
      createGame(value.textId);
    game.process(value);
  }

  switch(state) {
    case 'START_TIMER': return renderStartTimer(value.startTimerSecondsLeft);
    case 'GAME': return gameHandler(value);
    case 'GAME_FINISHED': return game.end(value);
  }
}

export const renderStartTimer = newTimerValue => {
  domHelper
    .hideById('room-back-button')
    .hideById('room-ready-button')
    .showById('room-start-timer', 'flex')
    .setInnerHTMLById('room-start-timer', newTimerValue)
}

export const renderComment = comment => {
  domHelper
    .setInnerHTMLById('room-commentator-text', comment)
}