export const createElement = ({ tagName, className, attributes = {} }) => {
  const element = document.createElement(tagName);

  if (className) {
    addClass(element, className);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
};

export const addClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.add(...classNames);
};

export const removeClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.remove(...classNames);
};

export const formatClassNames = className => className.split(" ").filter(Boolean);

export const highlight = (text, symbolsNumber) => {
  if (symbolsNumber >= 0) { 
   text = 
    '<span class="highlight">'
    + text.substring(0, symbolsNumber)
    + '</span>'
    + '<span class="underline">'
    + text.substring(symbolsNumber, symbolsNumber + 1) 
    + '</span>'
    + text.substring(symbolsNumber + 1);
  }

  console.log(text);
  return text;
}

class DomHelper {
  constructor(document) {
    this.document = document;
    return this;
  }

  hideById(stringId) {
    const element = document.getElementById(stringId);
    element.style.display = 'none';
    return this;
  }

  showById(stringId, display = 'block') {
    const element = document.getElementById(stringId);
    element.style.display = display;
    return this;
  }

  setStylepropertyById(stringId, styleProperty = '', value = '') {
    const element = document.getElementById(stringId);
    element.style[styleProperty] = value;
    return this;
  }

  setInnerHTMLById(stringId, innerHTML) {
    const element = document.getElementById(stringId);
    element.innerHTML = innerHTML;
    return this;
  }
  
  appendById(stringId, elements) {
    const element = document.getElementById(stringId);
    element.append(...elements);
    return this;
  }
}

export const domHelper = new DomHelper(document);